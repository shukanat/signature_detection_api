FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

#Linux libraries install
RUN \
    apt-get update \
    && apt-get install -y \
    apt-utils \
    software-properties-common \
    && add-apt-repository "deb http://security.ubuntu.com/ubuntu xenial-security main" \
    && apt-get install -y \
    autoconf \
    automake \
    cmake \
    pkg-config \
    libavcodec-dev \
    libavformat-dev \
    libswscale-dev \
    libtool \
    build-essential \
    libopencv-dev \
    libv4l-dev \ 
    libxvidcore-dev \
    libx264-dev \
    libjpeg-dev \
    libpng-dev \
    libtiff-dev \
    openexr \
    libgtk-3-dev \
    gfortran \
    libatlas-base-dev \
    libtbb2 \
    libtbb-dev \
    libdc1394-22-dev \
    && apt-get autoremove \
    && apt-get clean -y 

RUN pip install --upgrade pip \
    && pip install gdown \
	&& gdown https://drive.google.com/uc?id=1sb3fzwVKvOO8hZVZmy8K3Dc4En85rsrz \
    && gdown https://drive.google.com/uc?id=1dRtfGd-25ui7joxRLHbRV2uK4YyXf7rx

COPY ./app /app
COPY requirements.txt .
RUN pip --no-cache-dir install -r requirements.txt