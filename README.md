# API for signature detection
## Quick start guide
1.  Download repository <br/>
`$git clone https://gitlab.com/shukanat/signature_detection_api.git` <br/>
`$cd signature_detection_api` <br/>

2.  Create working enviroment <br/>
`ptyhon3 -m venv api` <br/>
`$source api/bin/activate` <br/>

3.  Install requirements <br/>
`$pip install -r requirements.txt` <br/>

4.  Download model config and weights to the `app/` directory <br/>

```
gdown https://drive.google.com/uc?id=1sb3fzwVKvOO8hZVZmy8K3Dc4En85rsrz
gdown https://drive.google.com/uc?id=1dRtfGd-25ui7joxRLHbRV2uK4YyXf7rx
```

5.  Launch API <br/>
`$uvicorn main:app --reload --app-dir app` <br/>

6.  Your API will be avaible at **127.0.0.0:8000**. For the documentation look: **127.0.0.0:8000/docs**.

## Quick start with Docker 
1.  Build an image from Dockerfile <br/>
`$sudo docker build -t signature-detection-api .` <br/>

2.  Create and start a new container <br/>
`$sudo docker run -d --name web-api -p 80:80 signature-detection-api` <br/>

3.  Your API will be avaible at **localhost**.

## Test
Check `test.ipynb` for the test.

